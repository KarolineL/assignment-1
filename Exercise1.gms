* Assignment 1, Exercise 1 *
* Karoline Liisberg *

* I start by defining my sets
SETS
F       "Set of formations"                  /442, 352, 4312, 433, 343, 4321/
R       "Set of roles"                       /Gk, CDF, LB, RB, CMF, LW, RW, OMF, CFW, SFW/
P       "Set of players"                     /P1*P25/
Q(P)    "Subset of quality players"          /P13, P20, P21, P22/
S(P)    "Subset of strength players"         /P10, P12, P23/
;

*I define the tables which are given in the exercise description
TABLE
A(F,R) "Number of players required for each formation and role"
            GK      CDF     LB      RB      CMF     LW      RW      OMF     CFW     SFW
442         1         2     1        1      2       1       1         0     2       0
352         1         3     0        0      3       1       1         0     1       1
4312        1         2     1        1      3       0       0         1     2       0
433         1         2     1        1      3       0       0         0     1       2
343         1         3     0        0      2       1       1         0     1       2
4321        1         2     1        1      3       0       0         2     1       0
;

TABLE
B(P, R) "Fitness player role"
        GK      CDF     LB      RB      CMF     LW      RW      OMF     CFW     SFW
P1      10      0        0      0       0       0       0       0       0       0
P2      0       0       0       0       0       0       0       0       0       0
P3      8.5     0       0       0       0       0       0       0       0       0
P4      0       8       6       5       4       2       2       0       0       0
P5      0       9       7       3       2       0       2       0       0       0
P6      0       8       7       7       3       2       2       0       0       0
P7      0       6       8       8       0       6       6       0       0       0
P8      0       4       5       9       0       6       6       0       0       0
P9      0       5       9       4       0       7       2       0       0       0
P10     0       4       2       2       9       2       2       0       0       0
P11     0       3       1       1       8       1       1       4       0       0
P12     0       3       0       2       10      1       1       0       0       0
P13     0       0       0       0       7       0       0       10      6       0
P14     0       0       0       0       4       8       6       5       0       0
P15     0       0       0       0       4       6       9       6       0       0
P16     0       0       0       0       0       7       3       0       0       0
P17     0       0       0       0       3       0       9       0       0       0
P18     0       0       0       0       0       0       0       6       9       6
P19     0       0       0       0       0       0       0       5       8       7
P20     0       0       0       0       0       0       0       4       4       10
P21     0       0       0       0       0       0       0       3       9       9
P22     0       0       0       0       0       0       0       0       8       8
P23     0       3       1       1       8       4       3       5       0       0
P24     0       3       2       4       7       6       5       6       4       0
P25     0       4       2       2       6       7       5       2       2       0
;

* I define my variables which I need to formulate and solve the problem
VARIABLES
z            "Objective value: ";

BINARY VARIABLES
x(P, R)      "which player p to employ and which role this player plays"
y(F)         "which formation chosen"
;

* I define the equations needed to formualte and solve the problem
EQUATIONS
Objective     "Obejctive function: Maximize the total fitness player-role"
Const1        "Constraint 1: Exactly one formation must be chosen"
Const2(R)     "Constraint 2: Which role r player p must play to cover the roles of the given formation"
Const3(P)     "Constraint 3: A player p can be employed in any role, but at most in one role r"
Const4        "Constraint 4: Ensures that at least one quality player is employed"
Const5        "Constraint 5: Ensures that if all quality players are employed at least one strength player must be employed"
;

* Objective function: maximizing the total fitness player role.
* I sum B(P,R)*x(P,R), such that I max the sum over the fitness of a player in a role r, only if the player p is employed to play role r.
Objective..     z   =e=  sum(P,sum(R, B(P, R)*x(P, R)));

* The sum of the formations must equal 1, such that only one formation is chosen.
Const1..    sum(F, y(f))    =E=  1;

* The sum of the players required for a formation f, must equal the number of players choosen for each and every role
Const2(R)..     sum(F, A(F,R)*y(F)) =e= sum(P, x(P, R));

* The sum of which role player p plays must be less than 1, such that player p is at most employed in one role.
Const3(P)..     sum(R, x(p, r))     =l=  1;

* The sum of the quality players must be greater or equal to one, such that we employ at least one quality player.
Const4..    sum(R, sum(Q(P), x(p, r)))  =g=     1;

* The sum of the quality players, P(Q), must be less than or equal to the sum of the strength players, P(S), plus the cardanality of Q minus 1,
* such that I employ at least one strength player if all quality players are employed.
* If all P(Q) are employed then LHS = card(P(Q)), then by subtracting 1 on the RHS, I force the sum of the P(S) to be at least 1 such that the inequality holds.
Const5..    sum(R, sum(Q(P), x(p,r)))   =l=  sum(S(P), sum(R, x(p,r) + card(Q)-1));

model Model1/all/;
solve Model1 using mip maximizing z;
display x.l, y.l, z.l;