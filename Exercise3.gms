* Assignment 1, Exercise 3 *
* Karoline Liisberg *

* I start by defining my sets
SETS
V         "The indexs of the given ships" /v1*v5/
P         "The indexs of the given Ports" /Singapore, Incheon, Shanghai, Sydney, Gladstone, Dalian, Osaka, Victoria/
R         "The indexs of the routes" /Asia, ChinaPacific/
H1(P)     "Subset one of the two incompatible ports" /Singapore, Osaka/
H2(P)     "Subset two of the two incompatible ports" /Incheon, Victoria/
;

* I define my parameteres
PARAMETERS
F(v)       "The cost when ship v is used" /v1 65, v2 60, v3 92, v4 100, v5 110/
G(v)       "The days a ship v can sail in a year" /v1 300, v2 250, v3 350, v4 330, v5 300/
D(p)       "If port p is visited, port p must be visited D(p) times in a year" /Singapore 15, Incheon 18, Shanghai 32, Sydney 32, Gladstone 45, Dalian 32, Osaka 15, Victoria 18/
;

*I Define the scalar K which is given in the exercise description
SCALAR
K "The number of ports Supersea must serve by contracts" /5/;

* I define the given tables from the exercise description
TABLE
C(v,r) "The cost for ship v to complete route r"
        Asia    ChinaPacific
v1      1.41    1.9
v2      3.0      1.5
v3      0.4      0.8
v4      0.5      0.7
v5      0.7      0.8
;

TABLE
T(v, r) "The time needed for ship v to complete route r"
        Asia    ChinaPacific
v1      14.4    21.2
v2      13.0    20.7
v3      14.4    20.6
v4      13.0    19.2
v5      12.0    20.1
;

TABLE
A(p,r) "gives 1 if route r passes through port p, 0 otherwise"
                Asia    ChinaPacific
Singapore       1         0
Incheon          1         0
Shanghai        1         1
Sydney           0         1
Gladstone       0         1
Dalian            1         1
Osaka            1         0
Victoria          0         1
;

* I define my four variables that I need to formulate and solve the problem
variables
z        "Objective value: total yearly cost";

binary variable
x(v)    "which ships to use: 1 if ship v is used, 0 otherwise"
y(p)    "which ports to serve; 1 if port p is visited, 0 otherwise"
;

integer variable
q(v,r)  "how many times ship v sails route r"
;

* The following equations are defined, to minimize the yearly cost, z.
equations
Objective       "The objective function which describes the total yearly cost"
Const1          "Constraint 1: The minimum of ports Supersea must serve by contracts"
Const2(P)       "Constraint 2: The minimum times port p must be visited, D(p), if port p is visited once."
Const3          "Constraint 3: Makes sure that only one of the ports in H1(p) is visited"
Const4          "Constraint 4: Makes sure that only one of the ports in H2(p) is visited"
Const5(V)       "Constraint 5: The time needed for ship v to sail route r once or multiply times must not exceed the days ship v can sail in a year"
;

* The objective function: The total yearly costs which I wish to minimize.
* Yearly costs are the sum of the cost of using ship v, F(v)*x(v), plus the sum of the cost of ship v completing route r, q times, C(v, r)*q(v, r).
Objective..     z       =e=     sum(V,  x(v) * F(v)) + sum(V,  sum(R,  C(v, r) * q(v, r)));

* Supersea must serve at least K ports by contracts, hence the sum over the ports of the ports visited must be grater or equal to K.
Const1..        sum(P,  y(p))    =G=     K;

* If port p is visited, port p must be visited D(p) times in a year.
* I sum over R and V of the times ship v sails route r, where route r passes through port p, which then gives me the times port p is visited one the LHS.
* Then LHS must be greater or equal to D(p) if port p is visited, y(p)*D(p).
Const2(P)..    sum(R,  sum(V,  A(p, r) * q(v, r)))   =G=    y(p) * D(p);

* The following two constraints, makes sure that we dont visit two ports that are incompatible ports to each other.
* See that the sum over H1(P) of ports visited in H1(P) must be less than or equal to 1. The same goes for H2(P).
Const3..        sum(H1(P),  y(p))       =L=     1;
Const4..        sum(H2(P),  y(p))       =L=     1;
    
* The time nedded for ship v to sail a route q times must not exceed the maximum days ship v can sail a year.
* Hence the sum over R of how long it takes for ship v to sail a route q times must be less than the days ship v can sail in a year.
Const5(V)..    sum(R,  T(v, r) * q(v, r))        =L=      G(v) * x(v);

model Model3/all/;
solve Model3 using mip minimizing z;
display x.l, y.l, q.l, z.l;