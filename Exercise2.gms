* Assignment 1, Exercise 2 *
* Karoline Liisberg  *

* I define my sets
SETS
K       "Set of keys" /k1*k30/
i       "Set of nodes i" /n1*n8/
;

* I define a alias of my set i, which i call j, such that I can interact with two different nodes in my set of nodes.
alias(i,j);

* I define the scalars as given in the exercise description 
SCALAR
mk      "Each key k occupies a space of mk"
Mi      "The memory limit of node i"
q       "Two nodes i and j must share at least q keys to be directly connected"
T       "Each key can at most be used T times"
;

mk = 186;
Mi = 1000;
q = 3;
T = 2;

* I define my variables which I need to formualte and solve the problem
VARIABLES
z       "Objective value: number of direct connections";

BINARY VARIABLES
x(k, i)     "Which keays, k, are assigned to each node i or j: equals 1 if node i has key k, 0 otherwise"
d(i, j)     "Equals 1 if node i and j are directly connected (shares q keys), 0 otherwise"
y(i, j, k)  "Equals 1 if node i and j shares key k"
;

* I define the equations I need to formulate and solve the problem
EQUATIONS
objective       	"The objective function: maximixinf the number of direct connections"
constr1         	"constraint 1: Keys assigned to a node i must not exceed the memory limit, Mi, of that node i"
constr2(k)      	"constraint 2: Each key, k, must at most be used T times"
constr3(i, j, k)    "constraint 3: Enures that y(i, j, k) equals 1 if node i and j shares key k"
constr4(i, j, k)    "constraint 4: Ensures that y(i, j, k) equals 0 if node i and j do not share key k"
constr5(i, j)       "constraint 5: Two nodes communicates directly if they share at least q keys"
;

*I want to maximize the number of direct connections, d(i, j).
objective..     	z   =e=	 sum(i, sum(j$(ord(i) > ord(j)), d(i,j)));

* For each node, i, the sum of what each key occupies of memory if assigned to node i shall be less than or equal to the memory capacity of node i, Mi.
constr1(i)..   		sum(k, mk*x(k,i))   =L=     Mi;

* The sum of each and every key k assigned to any node i shall be less than or equal to T, such that every key k is at most used T times.
constr2(k)..    	sum(i, x(k, i))     =L=     T;

*The following two constraints define y(i, j, k).
* If two nodes i and j shares key k then LHS equals 2, then since RHS must be larger or equal to LHS y(i, j, k) must equal 1.
constr3(i, j, k)..  x(k, i) + x(k, j)$(ord(i) > ord(j))     =L= 	 y(i, j, k) + 1;

* if node i and j do not share key k, then LHS equals eqither 1 or 0, hence y(i, j, k) must equal 0, such that 2*y(i, j, k)≤ LHS.
constr4(i, j, k)..  x(k, i) + x(k, j)$(ord(i) > ord(j))     =G=	 	 2*y(i, j, k);

* Defines d(i,j): if node i and j share at least q keys, they are directly connected:
* d(i, j) must equal 0, if node i and j, shares less than q keys, since the LHS then would be less than q, and LHS ≥ RHS. No direct connection.
* If node i and j shares more than q keys, d(i, j) can and will by the obj. func. equal 1. Node i and j are directly connected.
constr5(i, j)..     sum(K, y(i, j, k))  	=G=     q*d(i,j);

model Model2/all/;
solve Model2 using mip maximizing z;
display x.l, y.l, z.l;